# MeisterTask time parser

## Installation

Checkout the git repo. Run `npm install`.

## Usage

Execute `node index.js path/to/meistertask_time_tracker.csv`
Answer the question whether a task is a User Story or another task. The task types will be saved into a
 `*.json` file next to the time tracking csv file. Next time a specific time tracking csv is opened the 
 task types will be loaded from that file again.
