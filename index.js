var _ = require('lodash');
var parse = require('csv-parse');
var fs = require('fs');
var moment = require('moment');
var inquirer = require('inquirer');
var async = require('async');

var csvInputFile = process.argv[2];
if (!_.isString(csvInputFile)) {
    console.log('No Input file specified. Pass path to *.csv file as first command line argument');
    return;
}
fs.readFile(csvInputFile, 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }

    parse(data, {columns: true}, function (err, data) {
        var cleanData = data.map(function (timeLog) {
            timeLog.Date = moment(timeLog.Date, 'MM/DD/YYYY');
            timeLog.Hours = parseFloat(timeLog.Hours);
            return timeLog;
        });

        fs.readFile(csvInputFile + '.json', 'utf8', function (err, taskFileData) {
            var isTaskUserStory;
            if (err) {
                // Ignore the file not found error
                if (err.code !== 'ENOENT') {
                    return console.log(err);
                } else {
                    console.log('No existing task types found');
                    isTaskUserStory = {};
                }
            } else {
                console.log('Task types loaded successfully');
                isTaskUserStory = JSON.parse(taskFileData);
            }

            if (!_.isObject(isTaskUserStory)) {
                console.log('Task types file found but not in correct format');
                isTaskUserStory = {};
            }

            data.forEach(function (timeLog) {
                if (!_.isBoolean(isTaskUserStory[timeLog.Notes])) {
                    isTaskUserStory[timeLog.Notes] = null;
                }
            });

            // Ask for each task what type it is
            async.mapValuesSeries(isTaskUserStory, function (isUserStorie, taskName, cb) {
                if (_.isNull(isUserStorie) || !_.isBoolean(isUserStorie)) {
                    // We do not have information about this task yet, so ask the user
                    inquirer.prompt([
                        {
                            type: 'list',
                            name: 'type',
                            message: 'Is "' + taskName + '" a User Story?',
                            choices: [
                                'User Story',
                                'Other Task'
                            ]
                        }
                    ]).then(function (answers) {
                        return cb(null, answers.type === 'User Story');
                    });
                } else {
                    // We already know the type of that task
                    cb(null, isUserStorie);
                }
            }, function (err, results) {
                fs.writeFile(csvInputFile + '.json', JSON.stringify(results), function (err) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log('Task types saved successfully');

                    var timeLog = cleanData.reduce(function (acc, task, taskIndex) {

                        var weekStartMoment = moment(task.Date);
                        weekStartMoment.isoWeekday(1); // Set to monday
                        var weekStart = weekStartMoment.year() + '-' + weekStartMoment.isoWeek();
                        //console.log(task.Date.toISOString() + ' => ' + weekStart);

                        var currentWeek = acc.perWeek[weekStart];
                        if (_.isUndefined(currentWeek)) {
                            acc.perWeek[weekStart] = {userStories: 0, otherTasks: 0};
                            currentWeek = acc.perWeek[weekStart]
                        }
                        if (results[task.Notes]) {
                            currentWeek.userStories = currentWeek.userStories + task.Hours;
                        } else {
                            currentWeek.otherTasks = currentWeek.otherTasks + task.Hours;
                        }
                        var personName = task['First name'] + task['Last name'];
                        if (_.isUndefined(acc.perPerson[personName])) {
                            acc.perPerson[personName] = 0;
                        }
                        acc.perPerson[personName] += task.Hours;
                        return acc
                    }, {perWeek: {}, perPerson: {}});
                    // results is now an array of stats for each file
                    var sortedTimeLogs = Object.keys(timeLog.perWeek).map(function (key) {

                        var firstDayOfWeek = moment();
                        firstDayOfWeek.year(parseInt(key.split('-')[0], 10));
                        firstDayOfWeek.isoWeek(parseInt(key.split('-')[1], 10));
                        firstDayOfWeek.isoWeekday(1);
                        return {
                            week: firstDayOfWeek,
                            userStories: timeLog.perWeek[key].userStories,
                            otherTasks: timeLog.perWeek[key].otherTasks
                        };
                    }).sort(function (a, b) {
                        return a.week.unix() - b.week.unix();
                    });
                    console.log('\nWeek\tDate\t\t\tUser Stories\tOther Tasks\tTotal');
                    var overallSum = 0;
                    sortedTimeLogs.forEach(function (timeLogEntry) {
                        overallSum = overallSum + timeLogEntry.userStories + timeLogEntry.otherTasks;
                        var weekEnd = moment(timeLogEntry.week);
                        weekEnd.isoWeekday(7);
                        console.log(timeLogEntry.week.format('W\tD.M.-') + weekEnd.format('D.M.YY') + '\t\t' + timeLogEntry.userStories.toFixed(3) + 'h\t\t' + timeLogEntry.otherTasks.toFixed(3) + 'h\t\t' + (timeLogEntry.userStories + timeLogEntry.otherTasks).toFixed(3) + 'h')
                    });

                    console.log('Overall: ' + overallSum);
                    console.log(JSON.stringify(timeLog.perPerson, null, 2));
                });
            });
        });
    });
});

